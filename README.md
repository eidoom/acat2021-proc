# [acat2021-proc](https://gitlab.com/eidoom/acat2021-proc)

* [Live here](https://eidoom.gitlab.io/acat2021-proc/proc.pdf)
* Proceedings for [ACAT 2021 talk](https://gitlab.com/eidoom/acat2021)

## Licence
* Source code: GPLv3 (see LICENSE)
* Content, including figures and compiled document: CC BY 4.0 (see CC-BY-4.0)
