#include <cstddef>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <vector>

#include "model_fns.hpp"

// std::vector<float> read_amps(const std::string& filename)
// {
//   std::ifstream infile(filename);
//   std::string line;
//   std::vector<float> amps;

//   while (std::getline(infile, line)) {
//     amps.push_back(std::stof(line));
//   }

//   return amps;
// }

std::vector<std::vector<std::vector<float>>> read_moms(const std::string& filename)
{
  std::cout << "Reading momenta..." << '\n';

  std::ifstream infile(filename);
  std::string line, num;
  std::vector<std::vector<std::vector<float>>> points;

  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    std::vector<std::vector<float>> point(6, std::vector<float>(4));

    for (int i { 0 }; i < 6; ++i) {
      for (int j { 0 }; j < 4; ++j) {
        std::getline(iss, num, ' ');
        point[i][j] = std::stof(num);
      }
    }

    points.push_back(point);
  }

  return points;
}

// void run(const std::vector<std::vector<std::vector<float>>>& moms, const std::vector<float>& amps)
void run(const std::vector<std::vector<std::vector<float>>>& moms, double y_cut, const std::string& filename)
{
  std::cout << "Calculating points..." << '\n';

  nn::FKSEnsemble<float> model(6, 20,
      "/home/ryan/git/n3jet_diphoton/models/4g2A/RAMBO/"
      "100k_unit_0001_fks/",
      y_cut, "cut_0.02/");

  std::ofstream o(filename, std::ios::app);
  o << std::scientific << std::setprecision(16);

  for (std::size_t i { 0 }; i < moms.size(); ++i) {
    std::cout << i << '\n';
    model.compute(moms[i]);
    o << model.mean() << '\n';
  }
}

int main(int argc, char* argv[])
{
  double y;
  std::string name;
  switch (argc) {
  case 3:
    name = argv[2];
  case 2:
    y = std::stod(argv[1]);
    break;
  default:
    y = 0.001;
    name = "data/nn_6pt_1M_0.001.csv";
  }
  // std::vector<float> amps { read_amps("data/amp_6pt_1M.csv") };
  std::vector<std::vector<std::vector<float>>> moms { read_moms("data/mom_6pt_1M.csv") };
  // run(moms, amps);
  run(moms, y, name);
}
