#!/usr/bin/env python3

import csv, itertools
import numpy

if __name__ == "__main__":
    amps = numpy.load("events_1M_new_sherpa_cuts_PDF_loop.npy")
    with open("amp_6pt_1M.csv", "w") as csvfile:
        csvfile.write("\n".join(map(str, amps)) + "\n")

    moms = numpy.load("momenta_events_1M_new_sherpa_cuts_PDF.npy")
    with open("mom_6pt_1M.csv", "w", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=" ")
        writer.writerows(itertools.chain.from_iterable(mom) for mom in moms)
