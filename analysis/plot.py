#!/usr/bin/env python3

import argparse, pathlib, operator, math
import numpy, matplotlib.pyplot, matplotlib.cm, lhapdf


def load(name, refresh_cache=False):
    cache = f"{name}.npy"

    if refresh_cache or not pathlib.Path(cache).exists():
        data = numpy.genfromtxt(f"{name}.csv")
        numpy.save(cache, data)
        return data

    return numpy.load(cache, allow_pickle=True)


def dot(a, b):
    return a[0] * b[0] - sum(map(operator.mul, a[1:], b[1:]))


def min_yij(moms):
    d = 4
    n = len(moms) // d
    s01 = dot(moms[:d], moms[d : 2 * d])
    return min(
        dot(moms[i * d : (i + 1) * d], moms[j * d : (j + 1) * d]) / s01
        for i in range(n)
        for j in range(2 if i == 0 else i + 1, n)
    )


def load_min_yijs(name, moms=None, refresh_cache=False):
    cache = f"{name}_min_sij.npy"

    if refresh_cache or not pathlib.Path(cache).exists():
        min_yijs = numpy.array(
            [
                min_yij(x)
                for x in (moms if moms is not None else load(name, refresh_cache))
            ]
        )
        numpy.save(cache, min_yijs)
        return min_yijs

    return numpy.load(cache)


def load_region_indices(name, y, min_yijs, refresh_cache=False):
    cache_div = f"{name}_div_{y}.npy"
    cache_nondiv = f"{name}_nondiv_{y}.npy"

    if refresh_cache or not (
            pathlib.Path(cache_div).exists() or pathlib.Path(cache_nondiv).exists()
    ):
        div = []
        nondiv = []

        for i, yij in enumerate(min_yijs):
            if yij < y:
                div.append(i)
            else:
                nondiv.append(i)

        div = numpy.array(div, dtype=int)
        nondiv = numpy.array(nondiv, dtype=int)

        numpy.save(cache_div, div)
        numpy.save(cache_nondiv, nondiv)

        return div, nondiv

    return numpy.load(cache_div), numpy.load(cache_nondiv)


def ratio(r, indices, weights=None, base=10):
    rr = r[indices]
    rp = rr[rr > 0]

    if base == 10:
        rl = numpy.log10(rp)
    elif base == math.e:
        rl = numpy.log(rp)
    else:
        rl = numpy.log(rp) / math.log(base)

    if weights is not None:
        region_weights = weights[indices]
        region_weights = [w for w, rat in zip(region_weights, rr) if rat > 0]
        return rl, region_weights

    return rl, None


def rel_diff(amp, nn, indices):
    diff = numpy.abs((nn[indices] - amp[indices]) / amp[indices])
    return numpy.log10(diff)


# def digits_plot(amp, nn, regions, y):
#     fig, ax = matplotlib.pyplot.subplots()

#     logd = [rel_diff(amp, nn, region) for region in regions]

#     ax.hist(
#         x=logd,
#         bins=50,
#         log=True,
#         label=[
#             r"$\mathcal{R}_{\mathrm{div}}$",
#             r"$\mathcal{R}_{\mathrm{non\mbox{-}div}}$",
#         ],
#         histtype="step",
#         stacked=False,
#     )

#     ax.set_xlim(-8, 8)

#     ax.set_xlabel(
#         r"$\mathrm{log}_{10}\left(\left(\mathrm{NN}-\mathrm{NJet}\right)/\mathrm{NJet}\right)$"
#     )
#     ax.set_ylabel("Frequency")

#     ax.legend(loc="upper left", frameon=False)

#     fig.savefig(f"digits-plot-{y}.pdf")


def load_pdfs(name, beam_energy, safe_ps_points, refresh_cache=False):
    cache = f"{name}_pdfs.npy"

    if refresh_cache or not pathlib.Path(cache).exists():
        pid = 21  # gluon
        q = 91.188

        pdf = lhapdf.mkPDF("NNPDF31_nlo_as_0118")

        pdf_weights = numpy.array(
            [
                pdf.xfxQ(pid, point[0] / beam_energy, q)
                * pdf.xfxQ(pid, point[4] / beam_energy, q)
                for point in safe_ps_points
            ]
        )

        numpy.save(cache, pdf_weights)

        return pdf_weights

    return numpy.load(cache)


def weighted(name, points, rats, min_yijs, y, refresh_cache=False):
    beam_energy = 500  # GeV
    safe_indices = []
    div = []
    nondiv = []
    j = 0
    for i, (point, min_yij) in enumerate(zip(points, min_yijs)):
        if point[0] < beam_energy and point[4] < beam_energy:
            safe_indices.append(i)

            if min_yij < y:
                div.append(j)
            else:
                nondiv.append(j)
            j += 1

    safe_indices = numpy.array(safe_indices)

    pdf_weights = load_pdfs(name, beam_energy, points[safe_indices], refresh_cache)

    return pdf_weights, rats[safe_indices], (numpy.array(div), numpy.array(nondiv))


def error_plot(
    r,
    regions,
    y,
    base=10,
    bins=50,
    logy=False,
    stacked=False,
    aspect_ratio=1.75,
    weights=None,
):
    filename = f"error-plot-{'' if base==10 else '-E'}{y}.pdf"
    # filename = f"error-plot-{'' if base==10 else '-E'}{y}-{'log' if logy else 'lin'}-{'stacked' if stacked else 'overlaid'}.pdf"
    print(f"Creating {filename}")

    w = 6.4
    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w / aspect_ratio))

    ax.tick_params(axis="x", labelsize=15, direction="in", top=True, which="both")
    ax.tick_params(axis="y", labelsize=15, direction="in", right=True, which="both")

    ax.ticklabel_format(axis="y", style="scientific", scilimits=(0, 0))

    logr0, region_weights0 = ratio(r, regions[0], weights=weights, base=base)
    logr1, region_weights1 = ratio(r, regions[1], weights=weights, base=base)

    logr = (logr0, logr1)
    region_weights = (region_weights0, region_weights1)

    colours = matplotlib.cm.tab10(range(2))

    if weights is not None:
        ax.hist(
            x=logr,
            bins=bins,
            log=logy,
            label=[
                r"Weighted $\mathcal{R}_{\mathrm{div}}$",
                r"Weighted $\mathcal{R}_{\mathrm{non\mbox{-}div}}$",
            ],
            histtype="stepfilled" if stacked else "step",
            stacked=stacked,
            weights=region_weights,
            density=True,
            color=colours,
        )

    ax.hist(
        x=logr,
        bins=bins,
        log=logy,
        label=[
            r"$\mathcal{R}_{\mathrm{div}}$",
            r"$\mathcal{R}_{\mathrm{non\mbox{-}div}}$",
        ],
        histtype="stepfilled" if stacked else "step",
        stacked=stacked,
        density=weights is not None,
        linestyle="dotted" if weights is not None else "solid",
        color=colours,
    )

    lim = 6 if base == 10 else 14
    ax.set_xlim(-lim, lim)

    ax.set_xlabel(
        r"$\mathrm{log}_{10}\left(\mathrm{NN}/\mathrm{NJet}\right)$",
        fontsize=17,
        labelpad=10,
    )

    # ax.axvline(numpy.median(logr[0]), color="orange")
    # ax.axvline(numpy.median(logr[1]), color="blue")
    # ax.axvline(numpy.mean(logr[0]), color="red")
    # ax.axvline(numpy.mean(logr[1]), color="green")

    ax.set_ylabel(
        "Normalised (weighted) frequency" if weights is not None else "Frequency",
        fontsize=17,
        labelpad=10,
    )

    ax.legend(
        loc="upper left",
        frameon=False,
        prop={"size": 17},
    )

    fig.savefig(filename, bbox_inches="tight")


# def scales_plot(scales, y):
#     fig, ax = matplotlib.pyplot.subplots()

#     ax.hist(
#         x=numpy.log10(scales),
#         bins=100,
#         histtype="stepfilled",
#         stacked=True,
#         density=True,
#         log=True,
#     )

#     ax.set_xlabel(r"$\mathrm{log}_{10}\left(\mathrm{min}(y_{ij})\right)$")
#     ax.set_ylabel("Density")

#     ax.axvline(numpy.log10(y), color="black")

#     fig.savefig(f"scales-plot-{y}.pdf")


# def matrix_element_plot(nn, amp, regions, s01s):
#     amp = amp / s01s
#     nn = nn / s01s
#     fig, ax = matplotlib.pyplot.subplots()

#     ax.hist(
#         x=[
#             numpy.log10(x)
#             for x in [*[amp[y] for y in regions], *[nn[y] for y in regions]]
#         ],
#         # x=[amp, nn],
#         label=["amp-div", "amp-nondiv", "nn-div", "nn-nondiv"],
#         bins=100,
#         histtype="step",
#         # density=True,
#         log=True,
#     )
#     # ax.set_xscale('log')

#     # ax.set_xlim(-20, 4)
#     ax.set_xlabel(r"$\mathrm{log}_{10}\left(|\mathcal{A}|^2/s_{12}\right)$")
#     ax.set_ylabel("Frequency")
#     ax.legend(loc="upper left")

#     # ax.axvline(numpy.log10(y), color="black")

#     fig.savefig(f"a2-plot.pdf")


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--refresh-cache", action="store_true")
    parser.add_argument("-y", type=float, default=1e-3)
    args = parser.parse_args()
    if (
        args.refresh_cache
        and not input("Are you sure you want to refresh the cache? [y] ") == "y"
    ):
        exit()
    return args


if __name__ == "__main__":
    args = get_args()

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    kin_name = "data/mom_6pt_1M"
    mom = load(kin_name, args.refresh_cache)
    min_yijs = load_min_yijs(kin_name, mom, args.refresh_cache)

    regions = load_region_indices(kin_name, args.y, min_yijs, args.refresh_cache)
    print(f"{round(len(regions[0]) / 1e4, 1)}% divergent")

    amp = load("data/amp_6pt_1M", args.refresh_cache)
    nn = load(f"data/nn_6pt_1M_{args.y}", args.refresh_cache)
    print(f"{len(amp)} points")

    # print(f"Amp mean: {numpy.mean(amp)}")
    # print(f"amp-div mean: {numpy.mean(amp[regions[0]])}")
    # print(f"amp-nondiv mean: {numpy.mean(amp[regions[1]])}")
    # print(f"NN mean: {numpy.mean(nn)}")
    # print(f"NN-div mean: {numpy.mean(nn[regions[0]])}")
    # print(f"NN-nondiv mean: {numpy.mean(nn[regions[1]])}")

    r = nn / amp
    # print(f"Lin mean: {numpy.mean(r)}")
    # print(f"Lin median: {numpy.median(r)}")
    # print(f"Log mean: {10**numpy.mean(numpy.log10(r[r>0]))}")
    # print(f"Log median: {10**numpy.median(numpy.log10(r[r>0]))}")

    # error_plot(r, regions, args.y)

    # phi = (1 + 5**0.5) / 2
    # for logy in (True, False):
    #     # error_plot(r, regions, args.y, math.e, logy, True, phi)
    #
    #     # for stacked in (True, False):
    #     stacked = False
    #     error_plot(r, regions, args.y, 10, logy, stacked, phi)

    # digits_plot(amp, nn, regions, args.y)

    # scales = load_min_yijs("data/mom_6pt_1M", args.refresh_cache)
    # scales_plot(scales, args.y)

    # moms = load("data/mom_6pt_1M")
    # s01s = [dot(mom[0], mom[1]) for mom in moms]
    # matrix_element_plot(nn, amp, regions, s01s)

    pdf_weights, safe_r, safe_regions = weighted(kin_name, mom, r, min_yijs, args.y)

    error_plot(safe_r, safe_regions, y=args.y, weights=pdf_weights)
